package com.hepta.mercado;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.hepta.mercado.model.Produto;
import com.hepta.mercado.repository.ProdutoRepository;

@SpringBootApplication
public class HeptaShopApplication implements CommandLineRunner {

	@Autowired
	private ProdutoRepository produtoRepository;

	public static void main(String[] args) {
		SpringApplication.run(HeptaShopApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Produto p1 = new Produto("Notebook", 5000.0, "Dell", 5);
		Produto p2 = new Produto("Mouse", 10.0, "Positivo", 78);
		Produto p3 = new Produto("Teclado", 46.0, "Dell", 7);
		Produto p4 = new Produto("Router", 145.8, "Positivo", 5);
		produtoRepository.saveAll(Arrays.asList(p1, p2, p3, p4));

	}

}

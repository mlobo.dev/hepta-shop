package com.hepta.mercado.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hepta.mercado.model.Produto;
import com.hepta.mercado.repository.ProdutoRepository;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository repository;

	public List<Produto> listAll() {
		return repository.findAll();
	}

	public Produto find(Long id) {
		return repository.findById(id).get();

	}

	public Produto save(Produto obj) {
		obj.setId(null);
		return repository.save(obj);
	}

	public Produto update(Produto obj) {
		Produto newObj = find(obj.getId());
		updateData(newObj, obj);
		return repository.save(newObj);
	}

	private void updateData(Produto newObj, Produto obj) {
		newObj.setNome(obj.getNome());
	}

	public void deleteById(Long id) {
		repository.deleteById(id);
	}
	
	public void delete(Produto obj) {
		repository.delete(obj);
	}

}

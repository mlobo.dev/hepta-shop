package com.hepta.mercado.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hepta.mercado.model.Produto;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Long> {

}
